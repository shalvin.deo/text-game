// Scenes define the game progression
function scene(message, actions) {
	return {
		message: message,
		actions: actions,
	};
}

// ---------------------------------------------
// The main game loop is this:
//  1. The scene is set to a `sceneId` (initially with "start").
//	2. A `message`, defined by the scene, is printed.
//	3. The player enters a `verb`.
//	4. The `verb` is looked up in the scene's list of `actions`, and the `sceneId` corresponding to the `action` is set as the next scene.
//	5. Repeat from 2.
//  

const scenes = {
	"_exampleSceneId": scene(
		`This is a placeholder message for an example scene.`,
		{
			"example verb": "go_to_this_sceneId",
			"a different verb": "go_to_this_other_sceneId",
		}),

	"start": scene(
		`"Welcome to Drake's IGA Rochedale South."

The carpark bitumen scorches the bottom of your feet as you get out of your lifted Hilux.
		`,
		{
			"go": "carpark_crossing",
			"enter": "carpark_crossing",
			"walk in": "carpark_crossing",
		}),

	"carpark_crossing": scene(
		`You begin to cross the zebra crossing as a silver '01 Ford Falcon Stationwagon approaches. It doesn't seem to be slowing down...`,
		{
			"run": "carpark_crossing_avoid",
		}),

	"carpark_crossing_avoid": scene(
		`You run. The crossing longer than you anticipated as the stationwagon looms closer. Your feet press into the sharp, searing bitumen.

You make it to the other side as the stationwagon hurtles past heading for the edge of the carpark.

It seems to be heading toward a police officer.`,
		{
			"watch": "carpark_crossing_watch_police",
			"warn": "carpark_crossing_warn_police",
			"shout": "carpark_crossing_warn_police",
			"yell": "carpark_crossing_warn_police",
		}),

	"carpark_crossing_watch_police": scene(
		`The officer dodges the stationwagon with his preternatural agility. The stationwagon plows through the family behind him and explodes into a ball of flame.`,
		{
		}),
};


// ---------------------------------------------


// The global text buffer so we can write to it from anywhere
let textBuffer = "";

function writeLine(line) {
	textBuffer += line + "\n";
}

function beginScene(id) {
	writeLine(scenes[id].message);
	writeLine("");
}

function main() {
	const textInput = document.querySelector("#text-input");
	const textOutput = document.querySelector("#text-display");
	
	let gameState = {
		sceneId: "start",
	};

	const onOutputEmpty = () => {
		textInput.disabled = false;
	};
	initTextRenderer(textOutput, onOutputEmpty);

	const onRequest = (request) => {
		console.debug("Request: ", request);

		textInput.disabled = true;

		const currentScene = scenes[gameState.sceneId];
		const action = currentScene.actions[request.verb];
		if (action === undefined) {
			writeLine("Nothing interesting happens.");
			writeLine("");
		} else {
			gameState.sceneId = action;
			beginScene(gameState.sceneId);
		}
	};
	initInput(textInput, onRequest, () => {});

	textInput.disabled = true;
	textInput.focus();

	beginScene(gameState.sceneId);
}

function initTextRenderer(outputTextarea, onOutputEmpty) {
	// Set up the render loop with the browser's animation system
	const renderLoop = () => {
		// Check if text buffer will be empty _after_ the next render.
		const willTextBufferBeEmpty = textBuffer.length == 1;

		render(outputTextarea);

		if (willTextBufferBeEmpty) {
			onOutputEmpty();
		}

		window.requestAnimationFrame(renderLoop);
	};

	window.requestAnimationFrame(renderLoop);
}

function render(outputTextarea) {
	// Render text 1 character at a time
	if (textBuffer.length > 0) {
		outputTextarea.innerHTML += textBuffer.charAt(0);
		textBuffer = textBuffer.slice(1);
	}
}

function initInput(inputTextarea, onRequest, onRequestEnd) {
	const promptString = "> ";

	const resetTextarea = () => {
		inputTextarea.value = promptString;
		inputTextarea.selectionEnd = promptString.length;
		inputTextarea.focus();
	};

	const onKeyDown = (event) => {
		if (event.key == "Enter") {
			event.preventDefault();

			const inputString = inputTextarea.value.slice(promptString.length);

			writeLine(`> ${inputString}`);

			const parsedRequest = parseInput(inputString);
			onRequest(parsedRequest);

			resetTextarea();
			onRequestEnd();
		}
	};
	inputTextarea.addEventListener("keydown", onKeyDown)

	resetTextarea();
}

function parseInput(input) {
	console.log(`User input: "${input}"`);

	// TODO: Add actual parsing
	const inputWords = input.split(",");
	
	if (inputWords == 0) {
		// No words, return early
		return;
	}

	return {
		verb: inputWords[0],
	};
}

main();


